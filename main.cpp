#include <iostream>
#include <fstream>

using namespace std;

struct{
    string name;
    string address;
}contact;

bool newaddress();
bool printbook();

int main(int argc, char *argv[])
{
    bool errorcheck = 0;
    if (argc == 3)
    {
        contact.name = argv[1];
        contact.address = argv[2];
        errorcheck = newaddress();
   }
    else
    {
        errorcheck = printbook();
    }
    
    
    if (errorcheck == 1)
    {
        cout << "an error has occured.\n";
    }
    
    return 0;
}

bool newaddress()
{
    ofstream output;
    output.open("addresses.txt", ios::app | ios::ate);
    if (!output)
    {
        output.close();
        return 1;
    }
    output << "name: " << contact.name << " address: " << contact.address << endl;
    output.close();
    return 0;
}

bool printbook()
{
    ifstream input;
    input.open("addresses.txt");
    if (!input)
    {
        input.close();
        return 1;
    }
    string line = "";
    while(!input.eof())
    {
        input >> line;
        cout << line << endl;
    }
    input.close();
    return 0;
}